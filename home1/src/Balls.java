import java.util.*;


public class Balls {

    public enum Color {green, red};

    public static void main(String[] param) {
        Color[] pallid = genBallz(50);

        printArray(pallid);

        reorder2(pallid);

        printArray(pallid);

    }

    public static void reorder(Color[] balls) {
        int redCount = 0;
        for (Color ball : balls) {
            if (ball.ordinal() == 1) {
                redCount++;
            }
        }
        if (redCount == balls.length && redCount == 0) {
            ;
        } else {
            for (int i = 0; i < redCount; i++) {
                balls[i] = Color.red;
            }
            for (int i = redCount; i < balls.length; i++) {
                balls[i] = Color.green;
            }
        }
    }

    // pallidevõrdlemiseklass
    static class ballsCompare implements Comparator<Color> {
        @Override
        public int compare(Color o1, Color o2) {
            return o2.ordinal() - o1.ordinal();
        }
    }
    // java API versioon
    public static void reorder2(Color[] balls) {
        Arrays.sort(balls, new ballsCompare());
    }

    // java algoritm kasutades lambdat...TODO
    public static void lambdaOrder(Color[] balls) {
        Arrays.sort(balls, (Color o1, Color o2) -> o2.ordinal() - o1.ordinal());
    }


    //prindib suvalise massiivi konsooli
    public static void printArray(Color[] massiiv) {

        for(int i = 0; i < massiiv.length; i++) {
            System.out.println((i + 1) + ": " + massiiv[i]);
        }
    }

    // meetod, mis genereerib suvaliste pallidega massiivi
    public static Color[] genBallz(int n) {
        Color[] ballz = new Color[n];
        Random r = new Random();
        for (int i = 0; i < n; i++) {
            if (r.nextInt() < 0.5) {
                ballz[i] = Color.green;
            } else {
                ballz[i] = Color.red;
            }
        }
        return ballz;
    }
}


