import java.util.LinkedList;

public class LongStack {


   private LinkedList<Long> linkedL;

   LongStack() {
      linkedL = new LinkedList();

   }
   public static void main (String[] argum) {
      LongStack test = new LongStack();
      test.push(23876);
      test.pop();
      System.out.println(test.stEmpty());
      test.push(234987234);
      test.push(230982349);
      long f = test.interpret("2 5 + 4 * 9");
      System.out.println(f);

   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      LongStack res = new LongStack();
      res.linkedL = (LinkedList<Long>) linkedL.clone();
      return res;
   }

   public boolean stEmpty() {
     return(linkedL.size() == 0);
   }

   public void push (long a) {
      linkedL.push(a);
   }

   public long pop() {
      if(linkedL.size() == 0) {
         throw new RuntimeException("Can't remove element from empty stack!");
      }
      return linkedL.pop();
   } // pop

   public void op (String s) {
      if(linkedL.size() < 2)
         throw new RuntimeException("Operatsiooni " + s + " tegemiseks ei ole piisavalt arve.");
      long nr2 = linkedL.pop();
      long nr1 = linkedL.pop();
      try {
         if (s.equals("+"))
               push(nr1 + nr2);
         if (s.equals("-"))
               push(nr1 - nr2);
         if (s.equals("*"))
               push(nr1 * nr2);
         if (s.equals("/"))
               push(nr1 / nr2);

      } catch (IllegalArgumentException ex) {
         System.out.println("Illegal operator: " + s + " Allowed operators are: + - * /");
      }

   }
  
   public long tos() {
      if(linkedL.size() == 0) {
         throw new RuntimeException("Cant check topmost element in empty stack!");
      }
      return linkedL.peek();
   }

   @Override
   public boolean equals (Object o) {
      if(linkedL.size() == ((LongStack)o).linkedL.size()) {
         for (int i = 0; i < linkedL.size(); i++) {
            if (linkedL.get(i) != ((LongStack) o).linkedL.get(i)) {
               return false;
            }
         }
         return true;
      }
      return false;
   }

   @Override
   public String toString() {
      StringBuffer sbuff = new StringBuffer();
      for(int i = linkedL.size()-1; i >= 0; i--) {
         sbuff.append(String.valueOf(linkedL.get(i)));
      }
      return sbuff.toString();
   }

   public static long interpret (String pol) {

      if(pol == null || pol.trim().isEmpty()) {
         throw new RuntimeException("Enter something at least.");
      }
      LongStack ls = new LongStack();
      String[] str = pol.trim().split("\\s+");
      int countNr = 0;
      int countOp = 0;

      for(String s : str) {

         if(isLong(s)) {
            ls.push(Long.parseLong(s));
            countNr++;
         }
         else {
            ls.op(s);
            countOp++;
            if(ls.stEmpty()) {
               throw new RuntimeException("Illegal argument: " + s + " in expression " + pol);
            }
         }
      }
      if(!((countOp+1) == countNr)) {
         throw new RuntimeException("Stack OOB (out of balance). In expression " + pol);
      }
      return ls.pop();
   }

   public static boolean isLong(String str)
   {
      try
      {
         long l = Long.parseLong(str);
      }
      catch(NumberFormatException e)
      {
         return false;
      }
      return true;
   }
}

