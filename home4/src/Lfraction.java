/**
 * Created by flowland on 13.10.16.
 */


import java.util.IllegalFormatException;
import java.util.LongSummaryStatistics;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;

/** This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
      Lfraction l = new Lfraction(568, 12536);
      Lfraction m = new Lfraction(3, 4);
      Lfraction k = new Lfraction(7, 5);
      System.out.println(l);
      System.out.println(l.reduceFract(l));
      System.out.println(m.minus(k));
      System.out.println(valueOf("-4/-8/4"));


      // TODO!!! Your debugging tests here
   }

   private long num;
   private long den;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      long div = comDiv(a, b);
      if(b < 0) {
         b = b*(-1);
         a = a*(-1);
      }
      if(div > 0) {
         num = a/div;
         den = b/div;
      }
      if (b == 0) {
         throw new RuntimeException("Can't divide by zero.");
      }
      num = a;
      den = b;
   }

   /** Public method to access the numerator field.
    * @return numerator
    */
   public long getNumerator() {
      return num;
   }

   /** Public method to access the denominator field.
    * @return denominator
    */
   public long getDenominator() {
      return den;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return this.num + "/" + this.den;
   }

   /** Equality test
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      return this.compareTo((Lfraction) m) == 0;
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Objects.hash(this.num, this. den);
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      Lfraction sum;
      long newD = this.den*m.den;
      long newN = this.num*(newD/this.den) + m.num*(newD/m.den);
      return new Lfraction(newN, newD);
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      long newN = this.num * m.getNumerator();
      long newD = this.den * m.getDenominator();
      Lfraction l = new Lfraction(newN, newD);
      return l.reduceFract(l);
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      if(this.num != 0) {
         Lfraction l = new Lfraction(this.den, this.num);
         return l;
      } else {
         throw new RuntimeException("Denominator of the inverted fraction can't be zero.");
      }
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(-this.num, this.den);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      Lfraction sum;
      long newD = this.den*m.den;
      long newN = this.num*(newD/this.den) - m.num*(newD/m.den);
      return new Lfraction(newN, newD);
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      if(m.num == 0) {
         throw new RuntimeException("Cant divide by zero brah (yet).");
      }
      Lfraction k = new Lfraction(m.den, m.num);
      return this.times(k);
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      long a = this.num*m.den;
      long b = this.den*m.num;
      int res = 0;
      if(a > b)
         res = 1;
      else if(a < b)
         res = -1;
      return res;
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(this.num, this.den);
   }

   /** Integer part of the (improper) fraction.
    * @return integer part of this fraction
    */
   public long integerPart() {
      return this.num/this.den;
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      long a = this.num/this.den;
      long b = this.num-(this.den*a);
      return new Lfraction(b, this.den);


   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return (double) this.num / (double) this.den;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      if (d > 0)
         return new Lfraction((int)(Math.round (f*d)), d);
      else
         throw new RuntimeException ("Denominator can't be less than 1.");
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      String[] arr;
      if(StringUtils.isBlank(s))
         throw new RuntimeException("You entered an empty string...");
      try {
         arr = s.split("/");
      } catch(Exception er) {
         throw new RuntimeException("Format of fraction must be: n/d where n and d " +
                 " are long integers separated by a forward slash");
      }
      try {
         Long.parseLong(arr[0]);
      } catch(Exception er) {
         throw  new IllegalArgumentException(arr[0] + " is not a valid long integer value for the numerator.");
      }
      try {
         Long.parseLong(arr[1]);
      } catch(Exception er) {
         throw new IllegalArgumentException(arr[1] + " is not a valid long integer value for the denominator.");
      }
      if(s.trim().length() > 3) {
         throw new IllegalArgumentException("Can't have more than 3 characters in expression. You entered: " + s);
      }
      return reduceFract(new Lfraction(Long.parseLong(arr[0]), Long.parseLong(arr[1])));
   }


   public static Lfraction reduceFract (Lfraction l) {
      long oldN = l.getNumerator();
      long oldD = l.getDenominator();
      Lfraction fract;
      long cd = comDiv(oldN, oldD);
      fract = new Lfraction(oldN/cd, oldD/cd);
      return fract;
   }
   public static long comDiv(long a, long b) {
      if (a == 0 || b == 0)
         return a + b;
      else
         return comDiv(b, a % b);
   }

}

