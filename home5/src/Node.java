
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Node {

   // SICP chapter 1.2.2 Tree Recursion
   // Data Structures and Algorithms 7.2.2 Preorder Traversal
   // http://www.java2blog.com/2014/07/binary-tree-inorder-traversal-in-java.html
   // Lõpuks osutus stacki kasutamine elegantsemaks lahenduseks,
   // aga rekursiivse meetodiga õnnestumine oli ka lähedal (välja kommenteeritud).

   private String name;
   private Node firstChild;
   private Node nextSibling;
   //private static LinkedList<String> list;

   Node (String n, Node d, Node r) {
      this.name = n;
      this.firstChild = d;
      this.nextSibling = r;
   }


   public static Node parsePostfix (String s) {
      validateString(s);
      Stack<Node> nodeStack = new Stack<>();
      Node root = new Node(null, null, null);
      int bc = 0;
      String[] list = s.split("");
      for(int i = 0; i < list.length; i++) {
         String ch = list[i].trim();
         if(ch.equals("(")) {
            bc++;
            nodeStack.push(root);
            root.firstChild = new Node(null, null, null);
            root = root.firstChild;
         } else if(ch.equals(",")) {
            if(bc == 0)
               throw new RuntimeException("Comma must be inside brackets..." + s);
            root.nextSibling = new Node(null, null, null);
            root = root.nextSibling;
         } else if(ch.equals(")")) {
            bc--;
            root = nodeStack.pop();
         } else {
            if(root.name == null)
               root.name = ch;
            else
               root.name += ch;
         }
      }
      return root;
   }


   // separate string validation method for catching some more exotic errors
   public static void validateString(String s) {

      if(s.contains(",,"))
         throw new RuntimeException("String " + s + " can't contain double comma.");
      if(s.contains("()"))
         throw new RuntimeException("String " + s + " can't contain empty subtree.");
      if(s.isEmpty())
         throw new RuntimeException("String can't be empty.");
      if(s.contains("(,"))
         throw new RuntimeException("String " + s + " can't have comma without 2 nodes in subtree.");
      if(s.contains("))"))
         throw new RuntimeException("String " + s + " can't have double right bracket in R-parenthetic representation.");
      //regex expression for alphabetical symbols, */+- and ) ( ,
      if(!s.matches("[\\w(),*+-/]+"))
          throw new RuntimeException("String contains illegal symbols: " + s );
   }


   // method for making the consumable list(stack)
//   public static void makeList(String s) {
//      String[] arr = s.split("");
//      list = new LinkedList<>();
//      for(String str : arr) {
//         list.addLast(str);
//      }
//   }


   // recursive method for building the tree from nodes
//   public static Node buildNodes(LinkedList<String> list, Node r) {
//      while(!list.isEmpty()) {
//         String str = list.pop();
//         if (str.equals("(")) {
//            r.firstChild = buildNodes(list, new Node(null, null, null));
//         } else if (str.equals(",")) {
//            r.nextSibling = buildNodes(list, new Node(null, null, null));
//         } else if (str.equals(")")) {
//            return r;
//         } else {
//            if (r.name == null) {
//               r.name = str;
//            } else {
//               r.name += str;
//            }
//         }
//      }
//      return r;
//   }


   public String leftParentheticRepresentation() {
      String string = "";
      if(this.name != null) {
         string += this.name;
      }
      if(this.firstChild != null) {
         string += "(";
         string += this.firstChild.leftParentheticRepresentation();
         string += ")";
      } if(this.nextSibling != null) {
            string += ",";
            string += this.nextSibling.leftParentheticRepresentation();
      }
      return string;
   }


   public static void main (String[] param) {
      String s = "(((G,H)D,E,(I)F)B";
      Node t = Node.parsePostfix (s);
      String v = t.leftParentheticRepresentation();
      System.out.println (s + " ==> " + v); // (((G,H)D,E,(I)F)B ==> B(D(G,H),E,F(I))
   }
}

